﻿using log4net;
using Microsoft.Extensions.Configuration;
using Microsoft.Web.Administration;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;

namespace IzendaLog4netAppPoolUtility
{
    public class App
    {
        #region Variables
        private readonly IConfigurationRoot _config;
        private static ILog _log = LogManager.GetLogger(typeof(App));
        #endregion

        #region CTOR
        public App(IConfigurationRoot config) => _config = config;
        #endregion

        #region Methods
        /// <summary>
        /// Internal method to get Log4netFilesToKeep
        /// </summary>
        /// <returns></returns>
        internal int GetLog4neFilesToKeepThreshold()
        {
            var num = _config.GetValue<int>("Log4netFilesToKeep");

            return num;
        }

        /// <summary>
        /// Internal method to clean up old log4net files
        /// </summary>
        /// <returns></returns>
        internal void CleanupLog4netFiles()
        {
            try
            {
                var directory = _config.GetValue<string>("Log4netDirectory");

                var dirInfo = new DirectoryInfo(directory);
                if (!dirInfo.Exists)
                {
                    throw new Exception("Unknown directory information");
                }

                var files = dirInfo.GetFiles();

                Array.Sort(files, delegate (FileInfo f1, FileInfo f2)
                {
                    return f2.LastAccessTime.CompareTo(f1.LastAccessTime);
                });

                var filesCount = files.Length;
                var filesToKeep = GetLog4neFilesToKeepThreshold();

                if (filesCount > filesToKeep)
                {
                    for (int i = filesToKeep; i <= filesCount - 1; i++)
                    {
                        if (files[i].Name != "izenda-log.log")
                            files[i].Delete();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Internal method to recycle app pool
        /// </summary>
        /// <returns></returns>
        internal void RecycleAppPool()
        {
            try
            {
                // Improvement: OS based - Linux support
                var appPoolName = _config.GetValue<string>("AppPoolName");

                using (ServerManager iisManager = new ServerManager())
                {
                    SiteCollection sites = iisManager.Sites;

                    foreach (Site site in sites)
                    {
                        if (site.Name == appPoolName)
                        {
                            iisManager.ApplicationPools[site.Applications["/"].ApplicationPoolName].Recycle();

                            _log.Info(site.Name + " Recylce completed");

                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Internal method to conduct periodic ping job
        /// </summary>
        /// <returns></returns>
        internal async Task<bool> PingAPI()
        {
            var url = _config.GetValue<string>("ApiUrl");

            var client = new HttpClient();
            HttpResponseMessage result;

            try
            {
                result = await client.SendAsync(new HttpRequestMessage(HttpMethod.Head, url));

                _log.Info("Ping Status: " + result.StatusCode + "  " + DateTime.Now);

                client = null;
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// Internal method to get log4net lvl
        /// </summary>
        /// <returns></returns>
        internal string GetLog4netLogLevel()
        {
            var level = _config.GetValue<string>("Log4netLevel");

            if (level != "ALL" && level != "DEBUG" && level != "INFO" && level != "WARN" && level != "ERROR" && level != "FATAL" && level != "OFF")
            {
                throw new Exception("Invalid Log4netLevel settings. Please check your logging levels allowed!");
            }

            return level;
        }

        /// <summary>
        /// Internal method to adjust Log4net level
        /// </summary>
        /// <returns></returns>
        internal void AdjustLog4netLogLevel()
        {
            try
            {
                var url = _config.GetValue<string>("Log4netConfigLocation");

                var doc = new XmlDocument();
                doc.Load(url);

                var log4netNode = doc.SelectSingleNode("/log4net"); // Izenda BI .NET Framework version case

                if (log4netNode != null)
                {
                    UpdateLogLevel(log4netNode);
                }
                else
                {
                    log4netNode = doc.SelectSingleNode("/configuration/log4net"); //  Izenda BI .NET Core version case

                    if (log4netNode != null)
                        UpdateLogLevel(log4netNode);
                    else
                        throw new Exception("There is no log4net setting in your config file");
                }

                doc.Save(url);

                _log.Info("log4net level is now: " + GetLog4netLogLevel());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void UpdateLogLevel(XmlNode log4netNode)
        {
            XmlAttribute threshold = log4netNode.Attributes["threshold"];

            threshold.Value = GetLog4netLogLevel();
        }
        #endregion
    }
}
