﻿using log4net;
using log4net.Repository.Hierarchy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml;

namespace IzendaLog4netAppPoolUtility
{
    class Program
    {
        #region Variables
        private static IConfigurationRoot _configuration;
        private static ILog _log = LogManager.GetLogger(typeof(App));
        #endregion

        #region Methods
        /// <summary>
        /// Main entry point
        /// </summary>
        /// <param name="args">Define which function will be used</param>
        /// <returns></returns>
        static async Task Main(string[] args)
        {
            var log4netConfig = new XmlDocument();
            log4netConfig.Load(File.OpenRead("log4net.config"));
            var repo = LogManager.CreateRepository(Assembly.GetEntryAssembly(), typeof(Hierarchy));
            log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);

            _log.Info("Izenda Utility Started");

            try
            {
                if (args.Length == 0)
                {
                    throw new Exception("Please add a parameter to proceed. e.g., L,C,R, or P");
                }

                var type = args[0];

                switch (type)
                {
                    case "-L":
                        {
                            _log.Info("Update Izenda BI log4net log level update started");
                            MinimizeLog4netLevel();
                            _log.Info("Update Izenda BI log4net log level update completed");

                            break;
                        }

                    case "-C":
                        {
                            _log.Info("Izenda BI Log4net file cleaner job started");
                            Log4netCleanup();
                            _log.Info("Log4net file cleaner job completed");

                            break;
                        }

                    case "-R":
                        {
                            _log.Info("Start Recyle AppPool");
                            RecycleAppPool();
                            _log.Info("Recyle AppPool finished");

                            _log.Info("Test PingJob started");
                            DoPingJob();
                            _log.Info("Test PingJob completed");

                            break;
                        }

                    case "-P":
                        {
                            _log.Info("PingJob started");
                            DoPingJob();
                            _log.Info("PingJob completed");

                            break;
                        }

                    default:
                        {
                            throw new Exception("Unknown argument has been invoked. Please check your parameter. e.g., L,C,R, or P");
                        }
                }

                _log.Info("Izenda Utility stopped");
                
                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                _log.Error("ATTN: App stopped due to following error: " + ex.Message);

                _log.Warn("Izenda Utility terminated due to error");

                Environment.Exit(1);
            }
        }

        /// <summary>
        /// Invoke PingEvent
        /// </summary>
        private static void DoPingJob()
        {
            var result = PingEvent();

            if (!result.Result)
                throw new Exception("ATTN: Pingjob failed!");
        }

        /// <summary>
        /// Subscribed event to do - keep invoking PingAPI call based on interval
        /// </summary>
        /// <returns></returns>
        private static async Task<bool> ProcessPingJob()
        {
            var serviceProvider = GetServiceCollection();

            return await serviceProvider.GetService<App>().PingAPI();
        }

        /// <summary>
        /// Call CleanupLog4netFiles based on interval from Task Scheduler
        /// </summary>
        private static void Log4netCleanup()
        {
            var serviceProvider = GetServiceCollection();

            serviceProvider.GetService<App>().CleanupLog4netFiles();
        }

        /// <summary>
        /// ProcessPingJob called every tick
        /// </summary>
        private static async Task<bool> PingEvent() => await ProcessPingJob();

        /// <summary>
        /// Call AdjustLog4netLogLevel to Adjust Log4net level based on user settings
        /// RecycleAppPool also called to make changes implemented
        /// </summary>
        private static void MinimizeLog4netLevel()
        {
            var serviceProvider = GetServiceCollection();

            serviceProvider.GetService<App>().AdjustLog4netLogLevel();
            serviceProvider.GetService<App>().RecycleAppPool();
        }

        /// <summary>
        /// Call Recyle App Pool methods.
        /// </summary>
        private static void RecycleAppPool()
        {
            var serviceProvider = GetServiceCollection();

            serviceProvider.GetService<App>().RecycleAppPool();
        }

        /// <summary>
        /// Service proverider build method
        /// </summary>
        /// <returns></returns>
        private static IServiceProvider GetServiceCollection()
        {
            ServiceCollection serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            return serviceCollection.BuildServiceProvider();
        }

        /// <summary>
        ///  Configure service and inject it
        /// </summary>
        /// <param name="serviceCollection"></param>
        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            _configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
                .AddJsonFile("appsettings.json", false)
                .Build();

            serviceCollection.AddSingleton(_configuration);
            serviceCollection.AddTransient<App>();
        }
        #endregion
    }
}
